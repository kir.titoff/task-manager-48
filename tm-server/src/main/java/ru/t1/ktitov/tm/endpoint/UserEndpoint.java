package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktitov.tm.api.service.IAuthService;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.api.service.dto.IUserDtoService;
import ru.t1.ktitov.tm.dto.request.user.*;
import ru.t1.ktitov.tm.dto.response.user.*;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserDtoService getUserService() {
        return this.serviceLocator.getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = getUserService().lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @NotNull final UserDTO user = getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final UserDTO user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final String email = request.getEmail();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @NotNull final UserDTO user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final UserDTO user = serviceLocator.getUserService().findOneById(userId);
        return new UserProfileResponse(user);

    }

}
