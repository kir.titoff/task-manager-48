package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktitov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    M findOneById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    M removeById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

}
