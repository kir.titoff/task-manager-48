package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.ISessionService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.service.model.SessionService;
import ru.t1.ktitov.tm.service.model.UserService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    private void compareSessions(@NotNull final Session session1, @NotNull final Session session2) {
        Assert.assertEquals(session1.getId(), session2.getId());
        Assert.assertEquals(session1.getUser().getId(), session2.getUser().getId());
        Assert.assertEquals(session1.getRole(), session2.getRole());
    }

    private void compareSessions(
            @NotNull final List<Session> sessionList1,
            @NotNull final List<Session> sessionList2) {
        Assert.assertEquals(sessionList1.size(), sessionList2.size());
        for (int i = 0; i < sessionList1.size(); i++) {
            compareSessions(sessionList1.get(i), sessionList2.get(i));
        }
    }

    @Before
    public void initData() {
        @NotNull final IUserService userService = new UserService(connectionService, propertyService);
        userService.add(USER1);
        userService.add(USER2);
        userService.add(ADMIN3);
    }

    @After
    public void tearDown() {
        @NotNull final IUserService userService = new UserService(connectionService, propertyService);
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN3.getId());
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_SESSION1);
        service.add(USER1_SESSION2);
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        compareSessions(USER1_SESSION2, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_SESSION1);
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUser().getId());
    }

    @Test
    public void addList() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.findAll().size());
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        compareSessions(USER1_SESSION2, service.findAll().get(1));
        compareSessions(USER1_SESSION3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_SESSION_LIST);
        service.set(USER2_SESSION_LIST);
        compareSessions(USER2_SESSION1, service.findAll().get(0));
        compareSessions(USER2_SESSION2, service.findAll().get(1));
        compareSessions(USER2_SESSION3, service.findAll().get(2));
    }

    @Test
    public void clear() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.findAll().size());
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
        service.add(USER1_SESSION_LIST);
        service.add(USER2_SESSION_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(3, service.findAll().size());
        compareSessions(USER2_SESSION1, service.findAll().get(0));
        compareSessions(USER2_SESSION2, service.findAll().get(1));
        compareSessions(USER2_SESSION3, service.findAll().get(2));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_SESSION_LIST);
        service.add(USER2_SESSION_LIST);
        compareSessions(USER1_SESSION_LIST, service.findAll(USER1.getId()));
        compareSessions(USER2_SESSION_LIST, service.findAll(USER2.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_SESSION1);
        service.add(USER2_SESSION1);
        Assert.assertTrue(service.existsById(USER1_SESSION1.getId()));
        Assert.assertTrue(service.existsById(USER2_SESSION1.getId()));
        Assert.assertFalse(service.existsById(USER1_SESSION2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_SESSION1);
        service.add(USER2_SESSION1);
        compareSessions(USER1_SESSION1, service.findOneById(USER1_SESSION1.getId()));
        compareSessions(USER2_SESSION1, service.findOneById(USER2_SESSION1.getId()));
        compareSessions(USER1_SESSION1, service.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareSessions(USER1_SESSION2, service.findOneById(USER1_SESSION2.getId()));
        compareSessions(USER2_SESSION1, service.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.findAll().size());
        service.remove(USER1_SESSION1);
        Assert.assertEquals(2, service.findAll().size());
        service.removeById(USER1_SESSION2.getId());
        Assert.assertEquals(1, service.findAll().size());
        compareSessions(USER1_SESSION3, service.findAll().get(0));
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.findAll().size());
        service.remove(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(2, service.findAll().size());
        service.removeById(USER1.getId(), USER1_SESSION2.getId());
        Assert.assertEquals(1, service.findAll().size());
        compareSessions(USER1_SESSION3, service.findAll().get(0));
    }

}
